// Nice work with this, Sam.
// Your application looks great and has the comments
// and functions I needed to see.  Things appear to work
// well with the exception that if I enter in garbage
// data, the results say I either won or lost or tied.
// I'm thinking they should either not display at all
// or say that the data entered was bad, like the alert does.
// Perhaps you could have added a div for that?
// 18/20

    //Start with array
var gamePieces = ["Rock", "Paper", "Scissors", "Dynamite"];

    // prompt the user for their throw
function ask(){ //had userThrow inside, probably don't need it
    // reset the div content

        document.getElementById('results').innerHTML = "";
        document.getElementById('computer').innerHTML = "";
        document.getElementById('player').innerHTML = "";

            //array length
        var game = gamePieces.length;
            // computer takes length and generates random selection from the array

        computerChoice = getRandomGamePiece(game);

        function getRandomGamePiece(max){
                return Math.floor(Math.random() * Math.floor(max));
        }

            // give a value throw to the value that the computer generated.
        computerThrow = gamePieces[computerChoice];

        userThrow = prompt("Do you choose Rock, Paper, Scissors, or Dynamite ?");
        userThrow = userThrow.toLocaleUpperCase();

        if (userThrow != null) {
                if (userThrow == "ROCK") {
                     userThrow = "Rock";
                }else if (userThrow == "PAPER") {
                     userThrow = "Paper";
                }else if (userThrow == "SCISSORS") {
                     userThrow = "Scissors";
                }else if (userThrow == "DYNAMITE") {
                     userThrow = "Dynamite";
                }else{
                    alert("I'm sorry it appears you entered an invalid option.");
                }
            }else{
                alert("I'm sorry it appears you entered an invalid option.");

                }
                whoWins(userThrow, computerThrow);
        }



    function whoWins(userThrow, computerThrow){

        if (computerThrow == "Rock") {
            if(userThrow == "Rock"){    //User throws rock
                $("#results").text("Game is a tie");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else if (userThrow == "Paper"){ //User throws Paper
                $("#results").text("You Win!");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else if (userThrow == "Scissors"){  //User throws Scissors
                $("#results").text("You Lose");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else {  //User throws Dynamite
                $("#results").text("You Win!");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }

            }
        if (computerThrow == "Paper") {
            if(userThrow == "Rock"){    //User throws rock
                $("#results").text("You Lose");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else if (userThrow == "Paper"){ //User throws Paper
                $("#results").text("Game is a tie");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else if (userThrow == "Scissors"){  //User throws Scissors
                $("#results").text("You Win!");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else {  //User throws Dynamite
                $("#results").text("You Win!");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }

            }
        if (computerThrow == "Scissors") {
            if(userThrow == "Rock"){    //User throws rock
                $("#results").text("You Win!");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else if (userThrow == "Paper"){ //User throws Paper
                $("#results").text("You Lose");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else if (userThrow == "Scissors"){  //User throws Scissors
                $("#results").text("Game is a tie");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else {  //User throws Dynamite
                $("#results").text("You Lose");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }

            }
        if (computerThrow == "Dynamite") {
            if(userThrow == "Rock"){    //User throws rock
                $("#results").text("You Lose");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else if (userThrow == "Paper"){ //User throws Paper
                $("#results").text("You Lose");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else if (userThrow == "Scissors"){  //User throws Scissors
                $("#results").text("You Win!");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }
            else {  //User throws Dynamite
                $("#results").text("Game is an explosive tie");
                document.getElementById('computer').innerHTML = "Computer: " + computerThrow;
                document.getElementById('player').innerHTML = "Player: " + userThrow;
                }

            }
    }
